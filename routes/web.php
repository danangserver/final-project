<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function()
{
    return App\Profile::find(1)->user;
}
);

Route::get('/', [
    'uses' => 'FrontEndController@index',
    'as' => 'index'
]);

Route::get('/contact', [
    'uses' => 'FrontEndController@contact',
    'as' => 'contact'
]);

Route::get('/team', [
    'uses' => 'FrontEndController@team',
    'as' => 'team'
]);

Route::get('/about', [
    'uses' => 'FrontEndController@about',
    'as' => 'about'
]);

Route::get('/faq', [
    'uses' => 'FrontEndController@faq',
    'as' => 'faq'
]);

Route::get('/allgenre', [
    'uses' => 'FrontEndController@allgenre',
    'as' => 'allgenre'
]);

Route::get('/results', function()
{
    $posts = \App\Post::where('title', 'like', '%'.request('query').'%')->get();

    return view('results')->with('posts', $posts)
                          ->with('settings', App\Setting::first())
                          ->with('title', 'Search Results : '.request('query'))
                          ->with('genres', App\Genre::take(5)->get())
                          ->with('query', request('query'));
});

Route::get('/post/{slug}', [
    'uses' => 'FrontEndController@singlepost',
    'as' => 'post.single'
]);

Route::get('/genre/{id}', [
    'uses' => 'FrontEndController@genre',
    'as' => 'genre.single'
]);

Route::get('/tag/{id}', [
    'uses' => 'FrontEndController@tag',
    'as' => 'tag.single'
]);

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    
    Route::get('/home', [
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);
    
    Route::get('/post/create', [
        'uses' => 'PostsController@create',
        'as' => 'post.create'
    ]);

    Route::post('/post/store', [
        'uses' => 'PostsController@store',
        'as' => 'post.store'
    ]);

    Route::get('/post/edit/{id}', [
        'uses' => 'PostsController@edit',
        'as' => 'post.edit'
    ]);

    Route::get('/post/index', [
        'uses' => 'PostsController@index',
        'as' => 'post.index'
    ]);

    Route::post('/post/update/{id}', [
        'uses' => 'PostsController@update',
        'as' => 'post.update'
    ]);

    Route::get('/post/delete/{id}', [
        'uses' => 'PostsController@destroy',
        'as' => 'post.delete'
    ]);

    Route::get('/post/deleted', [
        'uses' => 'PostsController@show',
        'as' => 'post.deleted'
    ]);

    Route::get('/post/restore/{id}', [
        'uses' => 'PostsController@restore',
        'as' => 'post.restore'
    ]);

    Route::get('/genre/create', [
        'uses' => 'GenresController@create',
        'as' => 'genre.create'
    ]);

    Route::post('/genre/store', [
        'uses' => 'GenresController@store',
        'as' => 'genre.store'
    ]);

    Route::get('/genre/index', [
        'uses' => 'GenresController@index',
        'as' => 'genre.index'
    ]);

    Route::get('/genre/edit/{id}', [
        'uses' => 'GenresController@edit',
        'as' => 'genre.edit'
    ]);

    Route::get('/genre/delete/{id}', [
        'uses' => 'GenresController@destroy',
        'as' => 'genre.delete'
    ]);

    Route::post('/genre/update/{id}', [
        'uses' => 'GenresController@update',
        'as' => 'genre.update'
    ]);

    Route::get('/tag/index', [
        'uses' => 'TagsController@index',
        'as' => 'tag.index'
    ]);

    Route::get('/tag/create', [
        'uses' => 'TagsController@create',
        'as' => 'tag.create'
    ]);

    Route::get('/tag/edit/{id}', [
        'uses' => 'TagsController@edit',
        'as' => 'tag.edit'
    ]);

    Route::post('/tag/store/', [
        'uses' => 'TagsController@store',
        'as' => 'tag.store'
    ]);

    Route::post('/tag/update/{id}', [
        'uses' => 'TagsController@update',
        'as' => 'tag.update'
    ]);

    Route::get('/tag/delete/{id}', [
        'uses' => 'TagsController@destroy',
        'as' => 'tag.delete'
    ]);

    Route::get('/user/index',[
        'uses' => 'UsersController@index',
        'as' => 'user.index'
    ])->middleware('admin');

    Route::get('/user/create',[
        'uses' => 'UsersController@create',
        'as' => 'user.create'
    ]);

    Route::post('/user/store/',[
        'uses' => 'UsersController@store',
        'as' => 'user.store'
    ]);

    Route::get('/user/edit/{id}',[
        'uses' => 'UsersController@admin',
        'as' => 'user.edit'
    ]);

    Route::get('/user/not-admin/{id}',[
        'uses' => 'UsersController@not_admin',
        'as' => 'user.not.admin'
    ]);

    Route::post('/user/update/{id}',[
        'uses' => 'UsersController@update',
        'as' => 'user.update'
    ]);

    Route::get('/user/delete/{id}',[
        'uses' => 'UsersController@destroy',
        'as' => 'user.delete'
    ]);

    Route::get('/user/profile',[
        'uses' => 'ProfilesController@index',
        'as' => 'user.profile'
    ]);

    Route::post('/user/profile/update',[
        'uses' => 'ProfilesController@update',
        'as' => 'user.profile.update'
    ]);

    Route::get('/settings',[
        'uses' => 'SettingsController@index',
        'as' => 'settings'
    ]);

    Route::post('/settings/update',[
        'uses' => 'SettingsController@update',
        'as' => 'settings.update'
    ]);
});