<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
            <h2>Employe List</h2>
            </div>
        </div>
            <form action="{{ route('create.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-3">
                        <label for="name">Name :</label>
                        <input type="text" name="name" id="" class="form-control">
                    </div>
                </div>
                <div class="row text-center">
                    <div class="form-group col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>