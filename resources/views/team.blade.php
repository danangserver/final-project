@extends('layouts.frontend')

@section('content')
    <div class="stunning-header stunning-header-bg-lightviolet">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Our Team</h1>
        </div>
    </div>

    <!-- Post Details -->
	<div class="container">
		<div class="row medium-padding80">
            <div class="container">
                    <main class="main">    
                        <div class="row">
                            <div class="case-item-wrap">
                                @foreach ($teams as $team)
                                    <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
                                        <div class="case-item">
                                            <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                                                <img src="{{ $team->profile->avatar }}" alt="our case">
                                            </div>
                                            <a href="#"><h6 class="case-item__title">{{ $team->name }}</h6></a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </main>
            </div>
		</div>
	</div>
    <!-- End Post Details -->
@endsection
