@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel panel-heading text-center">
                <h4>List Users</h4>
            </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Image</th>
                        <th>Name</th>
                        <th>Permissions</th>
                        <th>Change Permissions</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @if($users->count() > 0)
                    @foreach($users as $user)
                       <tr>
                            <td class="text-center"><img src="{{ asset($user->profile->avatar) }}" alt="{{ $user->name }}" width="60px" height="60px" style="border-radius:50%;"></td>
                            <td>{{ $user->name }}</td>
                    
                            <td>
                                @if($user->admin == 0)
                                    Super Admin
                                @elseif($user->admin == 1)
                                    Admin
                                @elseif($user->admin == 2)
                                    Writer
                                @else
                                    User
                                @endif
                            </td>

                            <td align="left">
                                @if($user->admin == 0)
                                    @if($user->id > 1)
                                        <a href="{{ route('user.not.admin', ['id' => $user->id ]) }}" class="btn btn-xs btn-success">Make Default Admin</a>
                                    @endif
                                @elseif($user->admin == 1)
                                    <a href="{{ route('user.edit', ['id' => $user->id ]) }}" class="btn btn-xs btn-info">Make Super Admin</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td align="left">
                                @if($user->id > 1)
                                    <a href="{{ route('user.delete', ['id' => $user->id ]) }}" class="btn btn-xs btn-danger">Delete</a>    
                                @endif
                            </td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <td colspan="5" class="text-center">No post yet.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            </div>
        </div>

@stop