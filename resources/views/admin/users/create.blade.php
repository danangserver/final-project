@extends('layouts.app')

@section('content')

    @if(count($errors) > 0)

        <ul class="list-group">
        
            @foreach($errors->all() as $error)

                <li class="list-group-item text-danger">
                
                    {{$error}}

                </li>

            @endforeach

        </ul>

    @endif

    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h4>Create New User</h4>
        </div>

        <div class="panel-body">
            <form action="{{ route('user.store') }}" method="post" type="multipart/file">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name :</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="name">Email :</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="permission">Select Permission :</label>
                    <select name="permission_id" id="permission" class="form-control">
                        <option value="0">Super Admin</option>
                        <option value="1">Default Admin</option>
                        <option value="2">Writter</option>
                        <option value="3">User</option>
                    </select>
                </div>  
                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Add User</button>
                    </div>
                </div>
            </form>    
        </div>
    </div>
@stop