@extends('layouts.app')

@section('content')

    @if(count($errors) > 0)

        <ul class="list-group">
        
            @foreach($errors->all() as $error)

                <li class="list-group-item text-danger">
                
                    {{$error}}

                </li>

            @endforeach

        </ul>

    @endif

    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h4>Edit User</h4>
        </div>

        <div class="panel-body">
            <form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name :</label>
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="name">Email :</label>
                    <input type="text" name="email" value="{{ $user->email }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="avatar">Avatar :</label>
                    <input type="file" name="avatar" class="form-control">
                </div>
                <div class="form-group">
                    <label for="name">Facebook Profile :</label>
                    <input type="text" name="facebook" value="{{ $user->profile->facebook }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="name">Instagram Profile :</label>
                    <input type="text" name="instagram" value="{{ $user->profile->instagram }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="about">About :</label><br>
                    <textarea name="about" id="about" value="{{ $user->profile->about }}" cols="100" rows="6">{{ $user->profile->about }}</textarea>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Profile</button>
                    </div>
                </div>
            </form>    
        </div>
    </div>
@stop