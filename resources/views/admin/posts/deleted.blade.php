@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel panel-heading text-center">
                <h4>List Post</h4>
            </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="col-lg-1">ID</th>
                        <th class="col-lg-1">Image</th>
                        <th class="col-lg-5">Title</th>
                        <th class="col-lg-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        @if($post->status == false)
                       <tr>
                            <td class="col-lg-1">{{ $post->id }}</td>
                            <td class="col-lg-1"><img src="{{ $post->featured }}" alt="{{ $post->title }}" width="50px" height="30px"></td>
                            <td class="col-lg-5">{{ $post->title }}</td>
                    
                            <td align="left" class="col-lg-1">
                                <a href="{{route('post.restore', ['id' => $post->id])}}" class="btn btn-xs btn-info">Restore</a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>

@stop