@extends('layouts.app')

@section('content')

    @if(count($errors) > 0)

        <ul class="list-group">
        
            @foreach($errors->all() as $error)

                <li class="list-group-item text-danger">
                
                    {{$error}}

                </li>

            @endforeach

        </ul>

    @endif

    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h4>Edit Post</h4>
        </div>

        <div class="panel-body">
            <form action="{{ route('post.update',['id' => $post->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title :</label>
                    <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                </div>
                <div class="form-group">
                    <label for="afective">Affective Score :</label>
                    <input type="text" name="affective" class="form-control" value="{{ $post->affective }}">
                </div>
                <div class="form-group">
                    <label for="cognitive">Cognitive Score :</label>
                    <input type="text" name="cognitive" class="form-control" value="{{ $post->cognitive }}">
                </div>     
                <div class="form-group">
                    <label for="psikomotor">Psikomotor Score :</label>
                    <input type="text" name="psikomotor" class="form-control" value="{{ $post->psikomotor }}">
                </div>
                <div class="form-group">
                    <label for="genre">Choose a Genre :</label>
                    <select name="genre_id" id="genre" class="form-control">
                        @foreach($genres as $genre)
                            <option value="{{ $genre->id }}"
                            
                            @if($post->genre->id == $genre->id)
                                selected
                            @endif
                            
                            >{{ $genre->name }}</option>
                        @endforeach
                    </select>
                </div>  
                <div class="form-group">
                    <label for="tag">Select a Tags :</label>
                    @foreach($tags as $tag)
                        <div class="checkbox">
                            <label><input type="checkbox" name="tags[]" value="{{ $tag->id }}" 
                            
                                @foreach($post->tags as $t)
                                    @if($tag->id == $t->id)
                                        checked
                                    @endif
                                @endforeach

                            >{{$tag->name}}</label>
                        </div>
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="description">Description :</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $post->description }}</textarea>
                </div> 
                <div class="form-group">
                    <label for="download">Link Download :</label>
                    <input type="text" name="download" class="form-control" value="{{ $post->download }}">
                </div> 
                <div class="form-group">
                    <label for="featured">Featured Image :</label>
                    <input type="file" name="featured" class="form-control">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Post</button>
                    </div>
                </div>
            </form>    
        </div>
    </div>
@stop

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js" defer></script>
    <script>
        $(document).ready(function() {
            $('#description').summernote();
        });
    </script>
@stop