@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel panel-heading text-center">
                <h4>List Post</h4>
            </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                @if($posts->count() > 0)
                    @foreach($posts as $post)
                       <tr>
                            <td>{{ $post->id }}</td>
                            <td><img src="{{ $post->featured }}" alt="{{ $post->title }}" width="50px" height="30px"></td>
                            <td>{{ $post->title }}</td>
                    
                            <td align="left">
                                <a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-xs btn-info">Edit</a>
                            </td>
                            <td align="left">
                                <a href="{{ route('post.delete', ['id' => $post->id ]) }}" class="btn btn-xs btn-danger">Delete</a>    
                            </td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <td colspan="5" class="text-center">No post yet.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            </div>
            <!-- <div class="container">
                <div class="row">
                    <div class="text-center">
                        <a href="{{ route('post.create' )}}" class="btn btn-info">Create New Post</a>
                    </div>
                </div>
            </div> -->
        </div>

@stop