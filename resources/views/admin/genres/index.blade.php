@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading text-center">
            <h4>Genres</h4>
        </div>
        <div class="panel panel-body">
            <table class="table table-hover">
            
                <thead>
                
                    <th>Genre Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                
                </thead>
                <tbody>
                
                    @foreach($genres as $genre)
                        @if($genre->status === 1)
                        <tr>
                        
                            <td>{{ $genre->name }}</td>

                            <td><a href="{{ route('genre.edit',['id' => $genre->id])}}" class="btn btn-xs btn-info">Edit</a></td>

                            <td><a href="{{ route('genre.delete',['id' => $genre->id])}}" class="btn btn-xs btn-danger">Delete</a></td>
                        
                        </tr>
                        @endif
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

@stop