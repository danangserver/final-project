@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading text-center">
            <h4>Tags List</h4>
        </div>
        <div class="panel panel-body">
            <table class="table table-hover">
            
                <thead>
                
                    <th class="col-lg-4">Tag Name</th>
                    <th class="col-lg-2">Edit</th>
                    <th class="col-lg-2">Delete</th>
                
                </thead>
                <tbody>
                
                    @foreach($tags as $tag)
                        @if($tag->status == 1)
                        <tr>
                        
                            <td>{{ $tag->name }}</td>

                            <td><a href="{{ route('tag.edit',['id' => $tag->id])}}" class="btn btn-xs btn-info">Edit</a></td>

                            <td><a href="{{ route('tag.delete',['id' => $tag->id])}}" class="btn btn-xs btn-danger">Delete</a></td>
                        
                        </tr>
                        @endif
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

@stop