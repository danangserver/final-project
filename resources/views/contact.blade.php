<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@extends('layouts.frontend')

@section('content')
    <div class="stunning-header stunning-header-bg-lightviolet">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Contact or Request</h1>
        </div>
    </div>

    <!-- Post Details -->
	<div class="container">
            <div class="container">
                <div class="row medium-padding80">
                    <main class="main">    
                        <div class="row">
                            <div class="case-item-wrap">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <form>
                                        <div class="form-group">
                                            <label for="nama" style="color: black;">Name</label>
                                            <input type="text" id="name" class="form-control" placeholder="Type Here...">
                                        </div>
                
                                        <div class="form-group">
                                            <label for="email" style="color: black;">Email</label>
                                            <input type="email" id="email" class="form-control" placeholder="Type Here...">
                                        </div>
                
                                        <div class="form-group">
                                            <label for="subject" style="color: black;">Subject</label>
                                            <input type="text" id="subject" class="form-control" placeholder="Type Here...">
                                        </div>
                
                                        <div class="form-group">
                                            <label for="message" style="color: black;">Messages</label>
                                            <textarea class="form-control" name="message" rows="5" placeholder="Type Here..."></textarea>
                                        </div>
    
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
	</div>
    <!-- End Post Details -->
@endsection
