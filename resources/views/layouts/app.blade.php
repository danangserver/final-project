<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.1.1.js') }}"></script>
    {{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>  --}}
    <script src="{{ asset('js/toastr.min.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">

    @yield('styles')


</head>
<body style=" padding-top : 60px; ">
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">

                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                        @guest
                        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </nav>


        <main class="py-4">
        <div class="container">
            <div class="row">
            @if(Auth::check())
                <div class="col-md-4">
                   <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <h4>Navigation</h4>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a href="{{ route('home') }}">Dashboard</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('user.profile') }}">My Profile</a>
                            </li>
                            @if(Auth::user()->admin < 3)
                            <li class="list-group-item">
                                <a href="{{ route('post.create') }}">Create New Post</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('genre.create') }}">Create New Genre</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('tag.create') }}">Create New Tag</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('user.create') }}">Create New User</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('post.index') }}">Posts</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('genre.index') }}">Genres</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('tag.index') }}">Tags</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('user.index') }}">Users</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('post.deleted') }}">Deleted Posts</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('settings') }}">Settings</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            @endif
                <div class="col-md-8">
                    @yield('content')    
                </div>
            </div>
        </div>
        </main>
    </div>

    <script>
        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}")
        @endif

        @if(Session::has('info'))
            toastr.success("{{ Session::get('info') }}")
        @endif
    </script>

    @yield('scripts')

</body>
</html>
