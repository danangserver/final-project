<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'site_name' => 'Game Technology',
            'address' => 'Surabaya, Indonesia',
            'contact_number' => '+6281 55880 0398',
            'contact_email' => 'danangsugi1997@gmail.com'
        ]);
    }
}
