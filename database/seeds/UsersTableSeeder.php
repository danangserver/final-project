<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Danang Sugiarto',
            'email' => 'danangsugi1997@gmail.com',
            'password' => bcrypt('d4n4w0lfg4nk'),
            'admin' => 0
        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.jpg',
            'about' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Libero sequi officiis rem? Molestiae, hic possimus?',
            'facebook' => 'facebook.com/alan.mazerani',
            'instagram' => 'instagram.com/danangsugiarto_tkj'
        ]);
    }
}
