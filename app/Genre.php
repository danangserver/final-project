<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Genre extends Model
{
    protected $fillable = ['name','status'];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
