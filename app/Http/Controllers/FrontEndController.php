<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Genre;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index')
            ->with('settings', Setting::first())
            ->with('genres', Genre::take(5)->get())
            ->with('first_post', Post::orderBy('created_at','desc')->first())
            ->with('second_post', Post::orderBy('created_at','desc')->skip(1)->take(1)->get()->first())
            ->with('third_post', Post::orderBy('created_at','desc')->skip(2)->take(1)->get()->first())
            ->with('second_genre', Genre::orderBy('created_at','asc')->skip(1)->take(1)->get()->first())
            ->with('third_genre', Genre::orderBy('created_at','asc')->skip(2)->take(1)->get()->first());
            //->with('posts', Post::take(3)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function about()
    {
        return view('about')->with('genres', Genre::all())
                            ->with('settings', Setting::first());
    }

    public function faq()
    {
        return view('faq')->with('genres', Genre::all())
                          ->with('settings', Setting::first());
    }

    public function allgenre()
    {
        return view('allgenre')->with('genres', Genre::all())
                              ->with('settings', Setting::first());
    }

    public function contact()
    {
        return view('contact')->with('genres', Genre::take(5)->get())
                              ->with('settings', Setting::first());
    }

    public function team()
    {
        // $user = User::where('admin', 0)->get();
        // dd($user);

        return view('team')->with('genres', Genre::take(5)->get())
                           ->with('settings', Setting::first())
                           ->with('teams', User::where('admin', 0)->get());
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $next_id = Post::where('id', '>', $post->id)->min('id');

        $prev_id = Post::where('id', '<', $post->id)->max('id');

        return view('single')->with('post', $post)
                             ->with('settings', Setting::first())
                             ->with('title', $post->title)
                             ->with('genres', Genre::take(5)->get())
                             ->with('next', Post::find($next_id))
                             ->with('prev', Post::find($prev_id))
                             ->with('tags', Tag::all());
    }

    public function genre($id)
    {
        $genre = Genre::find($id);

        return view('genre')->with('genre', $genre)
                            ->with('name', $genre->name)
                            ->with('genres', Genre::take(5)->get())
                            ->with('settings', Setting::first());
    }

    public function tag($id)
    {
        $tag = Tag::find($id);

        return view('tag')->with('tag', $tag)
                          ->with('name', $tag->name)
                          ->with('genres', Genre::take(5)->get())
                          ->with('settings', Setting::first());
    }
}
