<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Auth;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'permission_id' => 'required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'admin' => $request->permission_id,
            'password' => bcrypt('password')
        ]);

        $profile = Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.jpg'
        ]);

        Session::flash('success','You successfully created a user.');

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->id == Auth::user()->id)
        {
            Session::flash('info','Cannot perform this action.');
            return redirect()->back();
        }

        if($user->admin == Auth::user()->admin)
        {
            Session::flash('info','You do not have permission.');
            return redirect()->back();
        }

        $user->delete();

        Session::flash('success','You successfully delete a user.');
        return redirect()->back();
    }

    public function admin($id)
    {
        if(Auth::user()->admin > 0)
        {
            Session::flash('info',' You do not have permission.');
            return redirect()->back();
        }

        $user = User::find($id);

        $user->admin = 0;

        $user->save();

        Session::flash('success','You successfully make admin.');

        return redirect()->back();
    }

    public function not_admin($id)
    {
        if(Auth::user()->admin > 0)
        {
            Session::flash('info',' You do not have permission.');
            return redirect()->back();
        }

        $user = User::find($id);

        $user->admin = 1;

        $user->save();

        Session::flash('success','You successfully make admin.');

        return redirect()->back();
    }
}
