<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Genre;
use App\Tag;
use Session;
use Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index')->with('posts', Post::where('status',1)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::all();
        $tags = Tag::all();

        if($genres->count() == 0 || $tags->count() == 0)
        {
            Session::flash('info','You must have some genres before attempting to create a post.');

            return redirect()->back();
        }

        return view('admin.posts.create')->with('genres', $genres)
                                         ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'affective' => 'required',
            'cognitive' => 'required',
            'psikomotor' => 'required',
            'featured' => 'required|image',
            'download' => 'required',
            'description' => 'required',
            'genre_id' => 'required',
            'tags' => 'required'
        ]);

        $featured = $request->featured;

        $featured_new_name = time().$featured->getClientOriginalName();

        $featured->move('uploads/posts', $featured_new_name);

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'affective' => $request->affective,
            'cognitive' => $request->cognitive,
            'psikomotor' => $request->psikomotor,
            'download' => $request->download,
            'slug' => str_slug($request->title),
            'featured' => 'uploads/posts/'.$featured_new_name,
            'status' => true,
            'genre_id' => $request->genre_id,
            'user_id' => Auth::id()
        ]);

        $post->tags()->attach($request->tags);

        $post->save();

        Session::flash('success','You succesfully create a post.');
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.posts.deleted')->with('posts', Post::all());
    }

    public function restore($id)
    {
        $post = Post::find($id);

        $post->status = true;

        $post->save();
        
        Session::flash('success','Your succesfully restored the post.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.posts.edit')->with('post', Post::find($id))
                                       ->with('genres', Genre::all())
                                       ->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if($request->hasFile('featured'))
        {
            $featured = $request->featured;

            $featured_new_name = time().$featured->getClientOriginalName();
    
            $featured->move('uploads/posts', $featured_new_name);

            $post->featured = 'uploads/posts/'.$featured_new_name;
        }

        $post->title = $request->title;

        $post->description = $request->description;

        $post->affective = $request->affective;

        $post->cognitive = $request->cognitive;

        $post->psikomotor = $request->psikomotor;

        $post->download = $request->download;

        $post->genre_id = $request->genre_id;

        $post->save();

        $post->tags()->sync($request->tags);

        Session::flash('info','You succesfully updated the post.');
        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->status = false;

        $post->save();

        Session::flash('success','You succesfully deleted the post.');
        return redirect()->back();
    }
}
