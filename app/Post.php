<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Genre;

class Post extends Model
{

    protected $fillable = ['title', 'description', 'affective', 
                           'cognitive', 'psikomotor', 'download',
                           'featured', 'slug', 'status', 'genre_id','user_id'];

    public function getFeaturedAttribute($featured)
    {
        return asset($featured);
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\User'); 
    }

}
